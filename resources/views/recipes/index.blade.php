<x-main-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Index des recettes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <ul>
                    @foreach ($recipes as $aRecipe)
                    <section class="p-4">
                        <li class="p-2">
                            <a href="">{{ $aRecipe->title }}</a><br/>
                        {{ $aRecipe->short_description }}
                        </li>
                    </section>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-main-layout>
