<x-main-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Index des recettes') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <h1>Ajouter une recette</h1>

                    <form action="{{ route('recipes.store') }}" method="POST">
                        <div class="field">
                            <label for="title">Titre</label>
                            <input id="title" name="title" type="text" />
                        </div>

                        <div class="field">
                            <label for="slug">Slug</label>
                            <input id="slug" name="slug" type="text" />
                        </div>


                        <div class="field">
                            <label for="short_description">Description succinte</label>
                            <textarea id="short_description" name="short_description">Message par défaut ici.
    </textarea>
                        </div>


                        <div class="field">
                            <label for="order">Ordre</label>
                            <input id="order" name="order" type="number" />
                        </div>
                        <input type="submit" value="Ajouter" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    </x-mainlayout>

    <!--
title
slug
illustration_img
gallerie d'images
mots-clefs

short_description
order

steps : order, texte (dont ingrédient), illustration
astuce

-->
    </form>

    Boissons
    Friandises et Desserts
