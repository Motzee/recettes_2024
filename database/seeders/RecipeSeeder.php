<?php

namespace Database\Seeders;

use App\Models\Recipe;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $recipes = [
            [
                'slug' => 'visitandine_coco',
                'title' => 'Visitandine coco',
                'short_description' => 'Un excellent gâteau moelleux à souhait, au bon goût de noix de coco. Une recette de ma maman !'
            ],
            [
                'slug' => 'tiramisu_speculoos',
                'title' => 'Tiramisu au spéculoos',
                'short_description' => 'Une version revisitée du tiramisu'
            ],
        ];

        foreach($recipes as $index => $aRecipe) {
            $aRecipe['order'] = $index + 1;
            Recipe::factory()->create($aRecipe);
        }
    }
}
